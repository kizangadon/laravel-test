<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Default application route
Route::get('/', 'HomeController@index')->name('home');
//User authentication routes
Auth::routes();

Route::group(['middleware', 'auth'], function () {
    //Route to the contact overview/list page
    Route::get('/contacts', 'ContactController@index')->middleware('auth')->name('contacts');
    //Contact route group
    Route::group(['prefix' => 'contact'], function () {
        //Route to create a contact
        Route::post('/', 'ContactController@store')->middleware('auth')->name('create-contact');
        //Route to delete a contact
        Route::delete('/{contact}', 'ContactController@destroy')->middleware('auth')->name('delete-contact');
        //Route to access the view to create a contact
        Route::get('/create', 'ContactController@create')->middleware('auth')->name('create');
        //Route to view a contact
        Route::get('/{contact}', 'ContactController@show')->middleware('auth')->name('view');
        //Route to access the page to edit a contact
        Route::get('/{contact}/edit', 'ContactController@edit')->middleware('auth')->name('edit');
        //Route to update a contact
        Route::put('/{contact}', 'ContactController@update')->middleware('auth')->name('update-contact');
    });
    //Contact search route group
    Route::group(['prefix' => 'search'], function () {
        //Route to access the view to search for a contact
        Route::get('/', 'ContactController@search')->middleware('auth')->name('search');
        //Route to search for a contact
        Route::post('/', 'ContactController@find')->middleware('auth')->name('search-contact');
    });
});
