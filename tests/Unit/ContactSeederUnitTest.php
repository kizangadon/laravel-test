<?php

namespace Tests\Unit;

use ContactTableSeeder;

use App\Contact;

use Tests\TestCase;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactSeederUnitTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function theExpectedNumberOfContactsAreCreatedInDatabase_whenTheContactSeederIsExecuted(){
        //Arrange
        $contactSeeder = new ContactTableSeeder();

        $expectedNumberOfContacts = 20;
        //Act
        $contactSeeder->run();
        //Assert
        $this->assertEquals($expectedNumberOfContacts, Contact::all()->count());
    }
}
