<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GuestExceptionsFeatureTest extends FeatureTest
{
    /**
     * @test
     */
    public function a_guest_isRedirecedToTheLandingPage_whenTheyAttempToLoginOrRegisterWithoutTheDatabaseTablesBeingSetup(){
        //Arrange
        $data = factory(User::class)->raw();

        $expectedRedirectedPath = route('home');
        //Act
        $response = $this->post(route('login'), $data);
        //Assert
        $response->assertRedirect($expectedRedirectedPath);
        //Arrange

        $expectedRedirectedPath = route('home');
        //Act
        $response = $this->post(route('register'), $data);
        //Assert
        $response->assertRedirect($expectedRedirectedPath);
    }

    /**
     * @test
     */
    public function a_guest_isShowAnErrorMessageOnTheLandingPage_whenTheyAttempToLoginOrRegisterWithoutTheDatabaseTablesBeingSetup(){
        //Arrange
        $data = factory(User::class)->raw();

        $expectedErrorMesage = "Dear user, it seems as if your database environment is not setup yet or properly. Please follow the instructions on our landing page on how to boot up the application, thank you.";
        //Act
        $response = $this->post(route('login'), $data);
        $response = $this->get(route('home'));
        //Assert
        $response->assertSee($expectedErrorMesage);
        //Act
        $response = $this->post(route('register'), $data);
        $response = $this->get(route('home'));
        //Assert
        $response->assertSee($expectedErrorMesage);
    }

    /**
     * @test
     */
    public function a_guest_isRedirectedToTheExpectedPath_whenTheyRequestAnInvalidRoute(){
        //Arrange
        $expectedRidrectPath = route('home');
        //Act
        $response = $this->get('/invalid/route');
        //Assert
        $response->assertRedirect($expectedRidrectPath);
    }

    /**
     * @test
     */
    public function a_guest_isShownAnErrorMessageOnTheExpectedRedirectedView_whenTheyRequestAnInvalidRoute(){
        //Arrange
        $expectedErrorMesage = "Dear user, the path that you requested could not be found, please try again.";
        //Act
        $response = $this->get('/invalid/route');
        $response = $this->get(route('home'));
        //Assert
        $response->assertSee($expectedErrorMesage);
    }
}
