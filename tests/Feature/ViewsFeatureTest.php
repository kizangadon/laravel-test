<?php

namespace Tests\Feature;

use App\Contact;

use App\User;

use Tests\TestCase;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewsFeatureTest extends FeatureTest
{
    use RefreshDatabase, WithFaker;

    /**
     *@test
    **/
    public function a_guest_isRedirectedToTheLoginPage_whenTheyAttemptToViewAnyOtherViewBesidesLoginAndRegister(){
        //Arrange
        $expectedRedirectPath= route('login');
        //Act
        $response = $this->get(route('contacts'));
        //Assert
        $response->assertRedirect($expectedRedirectPath);
    }

    /**
     *@test
    **/
    public function a_LoggedInUser_isShownTheExpectedView_whenTheyRequestTheContactsOverviewPage(){
        $this->withoutExceptionHandling();
        //Arrange
        $this->signInAsUser();

        $expectedView = "contacts.index";
        //Act
        $response = $this->get('/contacts');
        //Assert
        $response->assertViewIs($expectedView);
    }

    /**
     *@test
    **/
    public function a_LoggedInUser_isShownTheExpectedView_whenTheyRequestTheCreateContactPage(){
        //Arrange
        $this->signInAsUser();

        $expectedView = "contacts.create";
        //Act
        $response = $this->get(route('create'));
        //Assert
        $response->assertViewIs($expectedView);
    }

    /**
     *@test
    **/
    public function a_LoggedInUser_isShownTheExpectedView_whenTheyRequestTheViewAContactPage(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->create();

        $expectedView = "contacts.show";
        //Act
        $response = $this->get(route('view', $contact->id));
        //Assert
        $response->assertViewIs($expectedView);
    }

    /**
     *@test
    **/
    public function a_LoggedInUser_isShownTheExpectedView_whenTheyRequestTheEditAContactPage(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->create();

        $expectedView = "contacts.edit";
        //Act
        $response = $this->get(route('edit', $contact->id));
        //Assert
        $response->assertViewIs($expectedView);
    }

    /**
     *@test
    **/
    public function a_LoggedInUser_isShownTheExpectedView_whenTheyRequestTheSearchContactPage(){
        $this->withoutExceptionHandling();
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->create();

        $expectedView = "contacts.search";
        //Act
        $response = $this->get(route('search', $contact->id));
        //Assert
        $response->assertViewIs($expectedView);
    }

    /**
     *@test
    **/
    public function the_landingPage_showsTheExpectedInstructions_forAGuestOnTheView(){
        //Arrange
        $expectedInstructions = [
            'CMS',
            'Welcome',
            'Guest',
            'kizangadon@bitbucket.org/kizangadon/laravel-test.git',
            'Contacts Management System',
            'Since you are viewing this page',
            'you have read the instructions',
            'allow you to do the following things',
            'Keep track of all your contacts',
            'Add a new contact to your contact list',
            'Update a contact\'s details in your contact list',
            'Remove a contact from your contact list',
            route('login'),
            route('register')
        ];
        //Act
        $response = $this->get(route('home'));
        //Assert
        foreach($expectedInstructions as $instruction){
            $response->assertSee($instruction);
        }
    }

    /**
     *@test
    **/
    public function the_landingPage_showsTheExpectedContent_forALoggedInUserOnTheView(){
        //Arrange
        $this->signInAsUser();

        $expectedContent = [
            'CMS',
            'href="'. route('contacts') .'">Contacts',
            'Welcome ' . $this->user->name,
            'Contacts Management System',
            'Welcome back to your dashboard',
            'You can quickly create a new contact by clicking',
            '<a href="'. route('create')  .'">here</a>',
            'You can quickly go to your contacts overview by clicking',
            '<a href="'. route('contacts')  .'">here</a>',
            'href="'. route('contacts')  .'">Contacts Overview</a>',
            'href="'. route('create')  .'">Create Contact</a>',
            route('logout'),
        ];
        //Act
        $response = $this->get(route('home'));
        //Assert
        foreach($expectedContent as $content){
            $response->assertSee($content);
        }
    }

    /**
     *@test
    **/
    public function a_LoggedInUser_isShownTheExpectedContentOnTheView_whenTheyRequestTheContactsOverviewPageAndNoContactsExist(){
        //Arrange
        $this->signInAsUser();

        $expectedCreateLink = route('create');
        //Act
        $response = $this->get('/contacts');
        //Assert
        $response->assertSee('Contacts Overview')
                 ->assertSee("Firstname")
                 ->assertSee("Lastname")
                 ->assertSee("Email")
                 ->assertSee("Contact")
                 ->assertSee($expectedCreateLink);
    }

    /**
     *@test
    **/
    public function a_LoggedInUser_isShownTheExpectedContentOnTheView_whenTheyRequestTheContactsOverviewPageAndContactsExist(){
        //Arrange
        $this->signInAsUser();

        $contacts = factory(Contact::class, 3)->create();

        $expectedCreateLink = route('create');
        $expectedEditLink = route('edit', [$contacts[0]->id]);
        $expectedViewLink = route('view', [$contacts[0]->id]);
        //Act
        $response = $this->get('/contacts');
        //Assert
        $response->assertSee('Contacts Overview')
                 ->assertSee($contacts[0]->firstname)
                 ->assertSee($contacts[1]->firstname)
                 ->assertSee($contacts[2]->firstname)
                 ->assertSee($expectedCreateLink)
                 ->assertSee($expectedEditLink)
                 ->assertSee($expectedViewLink);
    }

    /**
     *@test
    **/
    public function a_LoggedInUser_isShownTheExpectedNumberOfContactsWithPagination_whenTheyRequestTheContactsOverviewPageAndContactsExist(){
        //Arrange
        $this->signInAsUser();

        factory(Contact::class, 11)->create();

        $expectedContent = [
            '<ul class="pagination',
            '<li class="page-item"',
            '<span class="page-link"'
        ];
        //Act
        $response = $this->get(route('contacts'));
        //Assert
        foreach($expectedContent as $content){
            $response->assertSee($content);
        }
    }

    /**
     *@test
    **/
    public function a_LoggedInUser_isShownTheExpectedContentOnTheView_whenTheyRequestTheCreateContactPage(){
        //Arrange
        $this->signInAsUser();

        $expectedOpeningFormTag = "<form";
        $expectedClosingFormTag = "</form>";
        $expectedFormAction = "action=\"" . route('create-contact') . "\"";
        $expectedFormMethod = "method=\"POST\"";
        $expectedCsrfToken = "<input type=\"hidden\" name=\"_token\"";
        $expectedFirstnameField = "<input name=\"firstname\" type=\"text\"";
        $expectedLastnameField = "<input name=\"lastname\" type=\"text\"";
        $expectedGenderField = "<select name=\"gender\"";
        $expectedEmailField = "<input name=\"email\" type=\"email\"";
        $expectedContactNumberField = "<input name=\"contact_number\" type=\"text\"";
        $expectedCreateContactButtonStart = "<button type=\"submit\"";
        $expectedCreateContactButtonEnd = ">Create Contact</button>";
        //Act
        $response = $this->get(route('create'));
        //Assert
        $response->assertSee('Create New Contact')
                 ->assertSee($expectedOpeningFormTag)
                 ->assertSee($expectedClosingFormTag)
                 ->assertSee($expectedFormMethod)
                 ->assertSee($expectedFormAction)
                 ->assertSee($expectedCsrfToken)
                 ->assertSee($expectedFirstnameField)
                 ->assertSee($expectedLastnameField)
                 ->assertSee($expectedGenderField)
                 ->assertSee($expectedEmailField)
                 ->assertSee($expectedContactNumberField)
                 ->assertSee($expectedCreateContactButtonStart)
                 ->assertSee($expectedCreateContactButtonEnd);
    }

    /**
     *@test
    **/
    public function a_LoggedInUser_isShownTheExpectedContentOnTheView_whenTheyRequestTheSearchContactPage(){
        //Arrange
        $this->signInAsUser();

        $expectedContent = [
            'Search Contacts',
            'This page allows you to search',
            '<form method="POST"',
            '<input type="hidden" name="_token',
            'action="' . route('search-contact'),
            'Search Criteria',
            'value="firstname"',
            'value="lastname"',
            'value="email"',
            'value="contact_number"',
            '<button type="submit"',
            '>Search Contact</button>',
            'href="'. route('contacts'),
            'Return To Contacts Overview</a>'
        ];
        //Act
        $response = $this->get(route('search'));
        //Assert
        foreach($expectedContent as $content){
            $response->assertSee($content);
        }
    }

    /**
     *@test
    **/
    public function a_LoggedInUser_isShownTheExpectedContentOnTheView_whenTheyRequestTheViewContactPage(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->create();

        $expectedFirstname = $contact->firstname;
        $expectedLastname = $contact->lastname;
        $expectedGender = $contact->gender;
        $expectedEmail = $contact->email;
        $expectedContactNumber = $contact->contact_number;
        $expectedEditButton = "<a href=\"" . route('edit', [$contact->id]) . "\"";
        $expectedDeleteForm = "<form method=\"POST\" action=\"" . route('delete-contact', [$contact->id])  ."\">";
        $expectedDeleteFormCsrfToken = "<input type=\"hidden\" name=\"_token\"";
        $expectedDeleteFormMethod = "<input type=\"hidden\" name=\"_method\" value=\"DELETE\"";
        $expectedDeleteButtonStart = "<button type=\"submit\"";
        $expectedDeleteButtonEnd = ">Delete " . $contact->firstname . "</button>";
        //Act
        $response = $this->get(route('view', [$contact->id]));
        //Assert
        $response->assertSee('Contact')
                 ->assertSee($expectedFirstname)
                 ->assertSee($expectedLastname)
                 ->assertSee($expectedGender)
                 ->assertSee($expectedEmail)
                 ->assertSee($expectedContactNumber)
                 ->assertSee($expectedEditButton)
                 ->assertSee($expectedDeleteForm)
                 ->assertSee($expectedDeleteFormMethod)
                 ->assertSee($expectedDeleteFormCsrfToken)
                 ->assertSee($expectedDeleteButtonStart)
                 ->assertSee($expectedDeleteButtonEnd);
    }

    /**
     *@test
    **/
    public function a_LoggedInUser_isShownTheExpectedContentOnTheView_whenTheyRequestTheEditContactPage(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->create();

        $expectedOpeningFormTag = "<form";
        $expectedClosingFormTag = "</form>";
        $expectedFormAction = "action=\"" . route('update-contact', [$contact->id]) . "\"";
        $expectedFormMethod = "<input type=\"hidden\" name=\"_method\" value=\"PUT\"";
        $expectedCsrfToken = "<input type=\"hidden\" name=\"_token\"";
        $expectedFirstnameField = "<input name=\"firstname\" type=\"text\" value=\"". $contact->firstname  ."\"";
        $expectedLastnameField = "<input name=\"lastname\" type=\"text\" value=\"". $contact->lastname  ."\"";
        $expectedGenderField = "<select name=\"gender\" class=\"form-control\" disabled";
        $expectedGenderSelectedField = "<option value=\"" . $contact->gender . "\" selected>" . $contact->gender . "</option>";
        $expectedEmailField = "<input name=\"email\" type=\"email\" value=\"". $contact->email  ."\"";
        $expectedContactNumberField = "<input name=\"contact_number\" type=\"text\" value=\"". $contact->contact_number  ."\"";
        $expectedEditContactButtonStart = "<button type=\"submit\"";
        $expectedEditContactButtonEnd = ">Update Contact</button>";
        //Act
        $response = $this->get(route('edit', [$contact->id]));
        //Assert
        $response->assertSee('Edit Contact')
                 ->assertSee($expectedOpeningFormTag)
                 ->assertSee($expectedClosingFormTag)
                 ->assertSee($expectedFormMethod)
                 ->assertSee($expectedFormAction)
                 ->assertSee($expectedCsrfToken)
                 ->assertSee($expectedFirstnameField)
                 ->assertSee($expectedLastnameField)
                 ->assertSee($expectedGenderField)
                 ->assertSee($expectedGenderSelectedField)
                 ->assertSee($expectedEmailField)
                 ->assertSee($expectedContactNumberField)
                 ->assertSee($expectedEditContactButtonStart)
                 ->assertSee($expectedEditContactButtonEnd);
    }

    /**
     * @test
     */
    public function a_loggedInUser_isShowTheExpectedView_whenTheySearchForAContactThatDoesExist(){
        //Arrange
        $this->signInAsUser();

        $contacts = factory(Contact::class, 5)->create([
            'firstname' => $firstname = $this->faker()->firstName
        ]);

        $data = [
            'criteria' => 'firstname',
            'value' => $firstname
        ];

        $expectedView = 'contacts.search-results';
        //Act
        $response = $this->post(route('search-contact'), $data);
        //Assert
        $response->assertViewIs($expectedView);
    }

    /**
     * @test
     */
    public function theExpectedNumberOfContacts_areShownToTheUser_whenTheySearchForAContactThatDoesExist(){
        $this->withoutExceptionHandling();
        //Arrange
        $this->signInAsUser();

        $contacts = factory(Contact::class, 5)->create([
            'firstname' => $firstname = $this->faker()->firstName
        ]);

        $data = [
            'criteria' => 'firstname',
            'value' => $firstname
        ];

        $expectedContent = [
            'Search Results',
            'Your search returned 5 result(s)',
            $firstname,
            $contacts[0]->lastname,
            $contacts[1]->lastname,
            $contacts[2]->lastname,
            $contacts[3]->lastname,
            $contacts[4]->lastname,
            $contacts[0]->email,
            $contacts[1]->email,
            $contacts[2]->email,
            $contacts[3]->email,
            $contacts[4]->email,
            '>View</a>',
            'Edit</a>',
            'Return To Contacts Overview'
        ];
        //Act
        $response = $this->post(route('search-contact'), $data);
        //Assert
        foreach($expectedContent as $content){
            $response->assertSee($content);
        }
    }

    /**
     *@test
    **/
    public function a_loggedInUser_isRedirectedToTheExpectedPath_whenTheyAttemptToViewAContactThatDoesntExist(){
        //Arrange
        $this->signInAsUser();

        $expectedRedirectPath = route('contacts');
        //Act
        $response = $this->get(route('view', [1]));
        //Assert
        $response->assertRedirect($expectedRedirectPath);
    }

    /**
     *@test
    **/
    public function a_loggedInUser_isRedirectedToTheExpectedPath_whenTheyAttemptToUpdateAContactThatDoesntExist(){
        //Arrange
        $this->signInAsUser();

        $data = factory(Contact::class)->raw();
        $expectedRedirectPath = route('contacts');
        //Act
        $response = $this->get(route('update-contact', [1]), $data);
        //Assert
        $response->assertRedirect($expectedRedirectPath);
    }

    /**
     *@test
    **/
    public function a_loggedInUser_isRedirectedToTheExpectedPath_whenTheyAttemptToDeleteAContactThatDoesntExist(){
        //Arrange
        $this->signInAsUser();

        $expectedRedirectPath = route('contacts');
        //Act
        $response = $this->get(route('delete-contact', [1]));
        //Assert
        $response->assertRedirect($expectedRedirectPath);
    }

    /**
     *@test
    **/
    public function a_loggedInUser_receivesTheExpectedFlashMessage_whenTheyAttemptToViewAContactThatDoesntExist(){
        //Arrange
        $this->signInAsUser();

        $expectedFlashMessage = "Dear user, the contact you attempted to view could not be found.";
        //Act
        $response = $this->get(route('view', [1]));
        //Assert
        $response->assertSessionHas('flash_error', $expectedFlashMessage);
    }

    /**
     *@test
    **/
    public function a_loggedInUser_receivesTheExpectedFlashMessage_whenTheyAttemptToUpdateAContactThatDoesntExist(){
        //Arrange
        $this->signInAsUser();

        $data = factory(Contact::class)->raw();
        $expectedFlashMessage = "Dear user, the contact you attempted to update could not be found, please try again.";
        //Act
        $response = $this->put(route('update-contact', [1]), $data);
        //Assert
        $response->assertSessionHas('flash_error', $expectedFlashMessage);
    }

    /**
     *@test
    **/
    public function a_loggedInUser_receivesTheExpectedFlashMessage_whenTheyAttemptToDeleteAContactThatDoesntExist(){
        //Arrange
        $this->signInAsUser();

        $expectedFlashMessage = "Dear user, the contact you attempted to delete could not be found, please try again.";
        //Act
        $response = $this->delete(route('delete-contact', [1]));
        //Assert
        $response->assertSessionHas('flash_error', $expectedFlashMessage);
    }

    /**
     *@test
    **/
    public function a_loggedInUser_isShownTheExpectedFlashMessage_whenTheySuccessfullyCreateAContact(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->raw();

        $expectedFlashMessage = "Dear user, you created a new contact by the name of " . $contact['firstname'] . " " . $contact['lastname'] ." successfully";
        //Act
        $response = $this->post(route('create-contact'), $contact);

        $response = $this->get(route('view', [1]));
        //Assert
        $response->assertSee($expectedFlashMessage);
    }

    /**
     *@test
    **/
    public function a_loggedInUser_isShownTheExpectedFlashMessage_whenTheySuccessfullyUpdateAContact(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->create();

        $data = factory(Contact::class)->raw([
            'firstname' => $contact->firstname,
            'lastname' => $contact->lastname
        ]);

        $expectedFlashMessage = "Dear user, you updated a contact by the name of " . $contact->firstname . " " . $contact->lastname ." successfully";
        //Act
        $response = $this->put(route('update-contact', [$contact->id]), $data);

        $response = $this->get(route('view', [$contact->id]));
        //Assert
        $response->assertSee($expectedFlashMessage);
    }

    /**
     *@test
    **/
    public function a_loggedInUser_isShownTheExpectedFlashMessage_whenTheySuccessfullyDeleteAContact(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->create();

        $expectedFlashMessage = "Dear user, you deleted a contact by the name of " . $contact['firstname'] . " " . $contact['lastname'] ." successfully";
        //Act
        $response = $this->delete(route('delete-contact', [$contact->id]));

        $response = $this->get(route('contacts'));
        //Assert
        $response->assertSee($expectedFlashMessage);
    }
}
