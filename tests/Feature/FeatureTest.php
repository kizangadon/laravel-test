<?php

namespace Tests\Feature;

use App\User;

use Tests\TestCase;

abstract class FeatureTest extends TestCase
{
    protected $user;

    protected function createEmail($firstname){
        return $firstname . "@" . 'company' . ".com";
    }

    protected function signInAsUser(){
        $this->user = factory(User::class)->create();

        $this->actingAs($this->user);
    }
}
