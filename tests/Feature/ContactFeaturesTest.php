<?php

namespace Tests\Feature;

use App\Contact;
use App\User;
use Tests\TestCase;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactFeaturesTest extends FeatureTest
{
    use RefreshDatabase, WithFaker;

    /**
     *@test
    **/
    public function a_guest_isRedirectedToLoginPage_whenTheyAttemptToViewContactsOverviewPage(){
        //Arrange
        $expectedRedirectedRoute = route("login");
        //Act
        $response = $this->get('/contacts');
        //Assert
        $response->assertRedirect($expectedRedirectedRoute);
    }

    /**
     *@test
    **/
    public function a_guest_isRedirectedToLoginPage_whenTheyAttemptToViewContactCreationPage(){
        //Arrange
        $expectedRedirectedRoute = route("login");
        //Act
        $response = $this->get('/contact/create');
        //Assert
         $response->assertRedirect($expectedRedirectedRoute);
    }

    /**
     *@test
    **/
    public function a_guest_isRedirectedToLoginPage_whenTheyAttemptToCreateAContact(){
        //Arrange
        $data = [
            'firstname' => $firstname = $this->faker->firstNameMale,
            'lastname' => $lastname = $this->faker->lastName,
            'gender' => 'Male',
            'email' => $this->createEmail($firstname, $lastname),
            'contact_number' => $this->faker->e164PhoneNumber
        ];

        $expectedRedirectedRoute = route("login");
        //Act
        $response = $this->post('/contact', $data);
        //Assert
         $response->assertRedirect($expectedRedirectedRoute);
    }

    /**
     *@test
    **/
    public function a_guest_isRedirectedToLoginPage_whenTheyAttemptToUpdateAContact(){
        //Arrange
        $contact = factory(Contact::class)->create();

        $route = "/contact/" . $contact->id;

        $data = [
            'firstname' => $firstname = $this->faker->firstNameMale,
            'lastname' => $contact->lastname,
            'gender' => $contact->gender,
            'email' => $this->createEmail($firstname, $contact->lastname),
            'contact_number' => '0820978675'
        ];

        $expectedRedirectedRoute = route("login");
        //Act
        $response = $this->put($route, $data);
        //Assert
         $response->assertRedirect($expectedRedirectedRoute);
    }

    /**
     *@test
    **/
    public function a_guest_isRedirectedToLoginPage_whenTheyAttemptToDeleteAContact(){
        //Arrange
        $contact = factory(Contact::class)->create();
        $route = "/contact/" . $contact->id;

        $expectedRedirectedRoute = route("login");
        //Act
        $response = $this->delete($route);
        //Assert
         $response->assertRedirect($expectedRedirectedRoute);
    }

    /**
     * @test
     */
    public function a_contact_isCreated_whenTheCorrectDataIsGivenAndAUserIsLoggedIn(){
        //Arrange
        $this->signInAsUser();

        $data = [
            'firstname' => $firstname = $this->faker->firstNameMale,
            'lastname' => $lastname = $this->faker->lastName,
            'gender' => 'Male',
            'email' => $this->createEmail($firstname),
            'contact_number' => $this->faker->e164PhoneNumber
        ];
        //Act
        $this->post('/contact', $data);
        //Assert
        $this->assertDatabaseHas('contacts', $data);
    }

    /**
     *@test
    **/
    public function a_contact_canBeUpdated_whenTheRequestedContactExistsTheDataIsValidAndTheUserIsLoggedIn(){
        $this->withoutExceptionHandling();
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->create();

        $data = [
            'firstname' => $firstname = $this->faker->firstNameMale,
            'lastname' => $contact->lastname,
            'gender' => $contact->gender,
            'email' => $this->createEmail($firstname),
            'contact_number' => '0820978675'
        ];
        //Act
        $this->put(route('update-contact', [$contact->id]), $data);
        //Assert
        $this->assertDatabaseHas('contacts', [
            'id' => $contact->id,
            'firstname' => $firstname,
            'email' => $data['email'],
            'contact_number' => $data['contact_number']
        ]);
    }

    /**
     *@test
    **/
    public function a_contact_canBeDeleted_whenItExistsInDatabaseAndTheUserIsLoggedIn(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->create();
        //Act
        $this->delete(route('delete-contact', [$contact->id]));
        //Assert
        $this->assertDatabaseMissing('contacts', [
            'id' => $contact->id,
            'email' => $contact->email
        ]);
    }

    /**
     * @test
     */
    public function a_loggedInUser_isRedirectedToTheExpectedPath_whenTheyHaveSuccessfullyCreatedAContact(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->raw();

        $expectedRedirectedRoute = route("view", [1]);
        //Act
        $response = $this->post(route('create-contact'), $contact);
        //Assert
        $response->assertRedirect($expectedRedirectedRoute);
    }

    /**
     * @test
     */
    public function a_loggedInUser_isRedirectedToTheExpectedPath_whenTheyHaveSuccessfullyUpdatedAContact(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->create();

        $data = factory(Contact::class)->raw();

        $expectedRedirectedRoute = route("view", [1]);
        //Act
        $response = $this->put(route('update-contact', [$contact->id]), $data);
        //Assert
        $response->assertRedirect($expectedRedirectedRoute);
    }

    /**
     * @test
     */
    public function a_loggedInUser_isRedirectedToTheExpectedPath_whenTheyHaveSuccessfullyDeletedAContact(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->create();

        $expectedRedirectedRoute = route("contacts");
        //Act
        $response = $this->delete(route('delete-contact', [$contact->id]));
        //Assert
        $response->assertRedirect($expectedRedirectedRoute);
    }

    /**
     * @test
     */
    public function a_maximumOfFiveContactsAreReturned_whenALoggedInUserVisitsTheContactOverviewPage(){
        //Arrange
        $this->signInAsUser();

        $contacts = factory(Contact::class, 6)->create();
        //Act
        $response = $this->get(route('contacts'));
        //Assert
        $response->assertDontSee($contacts[5]->email);
    }
}
