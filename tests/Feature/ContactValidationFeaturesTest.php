<?php

namespace Tests\Feature;

use App\Contact;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactValidationFeaturesTest extends FeatureTest
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function aLoggedInUser_hasTheExpectedErrorsInSession_whenTheyAttemptToCreatAContactWithEmptyValues(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->raw([
            'firstname' => '',
            'lastname' => '',
            'gender' => '',
            'email' => '',
            'contact_number' => ''
            ]);

        $expectedErrors = [
            'firstname',
            'lastname',
            'gender',
            'email',
            'contact_number'
        ];
        //Act
        $response = $this->post('/contact', $contact);
        //Assert
        $response->assertSessionHasErrors($expectedErrors);
    }

    /**
     * @test
     */
    public function aLoggedInUser_hasTheExpectedErrorsInSession_whenTheyAttemptToCreatAContactWithInvalidData(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->raw([
            'firstname' => '0147',
            'lastname' => 'a',
            'gender' => 'Cat',
            'email' => 'fake email',
            'contact_number' => 'fake014'
            ]);

        $expectedErrors = [
            'firstname',
            'lastname',
            'gender',
            'email',
            'contact_number'
        ];
        //Act
        $response = $this->post('/contact', $contact);
        //Assert
        $response->assertSessionHasErrors($expectedErrors);
    }

    /**
     * @test
     */
    public function aLoggedInUser_hasTheExpectedErrorsInSession_whenTheyAttemptToUpdateAContactWithEmptyValues(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->create();

        $data = [
            'firstname' => '',
            'lastname' => '',
            'email' => '',
            'contact_number' => ''
        ];

        $expectedErrors = [
            'firstname',
            'lastname',
            'email',
            'contact_number'
        ];
        //Act
        $response = $this->put(route('update-contact', [$contact->id]), $data);
        //Assert
        $response->assertSessionHasErrors($expectedErrors);
    }

    /**
     * @test
     */
    public function aLoggedInUser_hasTheExpectedErrorsInSession_whenTheyAttemptToUpdateAContactWithInvalidData(){
        //Arrange
        $this->signInAsUser();

        $contact = factory(Contact::class)->create();

        $data = [
            'firstname' => 'b107',
            'lastname' => 'bo',
            'email' => '01475@',
            'contact_number' => '014752'
        ];

        $expectedErrors = [
            'firstname',
            'lastname',
            'email',
            'contact_number'
        ];
        //Act
        $response = $this->put(route('update-contact', [$contact->id]), $data);
        //Assert
        $response->assertSessionHasErrors($expectedErrors);
    }
}
