<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserExceptionsFeatureTest extends FeatureTest
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function a_loggedInUser_isRedirectedToTheExpectedPath_whenTheyRequestAnInvalidRoute(){
        //Arrange
        $this->signInAsUser();

        $expectedRidrectPath = route('home');
        //Act
        $response = $this->get('/invalid/route');
        //Assert
        $response->assertRedirect($expectedRidrectPath);
    }

    /**
     * @test
     */
    public function a_loggedInUser_isShownAnErrorMessageOnTheExpectedRedirectedView_whenTheyRequestAnInvalidRoute(){
        //Arrange
        $this->signInAsUser();

        $expectedErrorMesage = "Dear user, the path that you requested could not be found, please try again.";
        //Act
        $response = $this->get('/invalid/route');
        $response = $this->get(route('home'));
        //Assert
        $response->assertSee($expectedErrorMesage);
    }
}
