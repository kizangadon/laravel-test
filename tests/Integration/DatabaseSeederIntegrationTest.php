<?php

namespace Tests\Integration;

use DatabaseSeeder;

use App\Contact;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DatabaseSeederIntegrationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function contacts_AreCreatedInTheDatabase_whenTheDatabaseSeederIsExecuted(){
        //Arrange
        $databaseSeeder = new DatabaseSeeder();

        $expectedNumberOfContacts = 20;
        //Act
        $databaseSeeder->run();
        //Assert
        $this->assertEquals($expectedNumberOfContacts, Contact::all()->count());
    }
}
