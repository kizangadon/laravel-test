<?php

namespace Tests\Integration;

use App\Contact;
use App\Repository\ContactRepository;
use App\Service\ContactService;
use Tests\TestCase;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactServiceIntegrationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected $service;

    public function setUp(): void{
        parent::setUp();

        $this->service = app()->make(ContactService::class);
    }

    /**
     * @test
     */
    public function the_contactService_retrievesTheExpectedNumberOfContacts_whenTheySearchForAContactsFirstname_And_SuchContactExists(){
        //Arrange
        $contacts = factory(Contact::class, 10)->create([
            'firstname' => $this->faker()->firstName
            ]);

        $searchFirstname = $contacts[0]->firstname;
        $expectedNumberOfContacts = 10;
        //Act
        $retrievedContacts = $this->service->searchByFirstname($searchFirstname);
        //Assert
        $this->assertEquals($expectedNumberOfContacts, $retrievedContacts->count());
    }

    /**
     * @test
     */
    public function the_contactService_retrievesTheExpectedNumberOfContacts_whenTheySearchForAContactsLastname_And_SuchContactExists(){
        //Arrange
        $contacts = factory(Contact::class, 8)->create([
            'lastname' => $this->faker()->lastName
            ]);

        $searchLastname = $contacts[0]->lastname;
        $expectedNumberOfContacts = 8;
        //Act
        $retrievedContacts = $this->service->searchByLastname($searchLastname);
        //Assert
        $this->assertEquals($expectedNumberOfContacts, $retrievedContacts->count());
    }

    /**
     * @test
     */
    public function the_contactService_retrievesTheExpectedNumberOfContacts_whenTheySearchForAContactsEmail_And_SuchContactExists(){
        //Arrange
        $contacts = factory(Contact::class, 17)->create([
            'email' => $this->faker()->email
            ]);

        $searchEmail = $contacts[0]->email;
        $expectedNumberOfContacts = 17;
        //Act
        $retrievedContacts = $this->service->searchByEmail($searchEmail);
        //Assert
        $this->assertEquals($expectedNumberOfContacts, $retrievedContacts->count());
    }

    /**
     * @test
     */
    public function the_contactService_retrievesTheExpectedNumberOfContacts_whenTheySearchForAContactsContactNumber_And_SuchContactExists(){
        //Arrange
        $contacts = factory(Contact::class, 17)->create([
            'contact_number' => $this->faker()->e164PhoneNumber
            ]);

        $searchContactNumber = $contacts[0]->contact_number;
        $expectedNumberOfContacts = 17;
        //Act
        $retrievedContacts = $this->service->searchByContactNumber($searchContactNumber);
        //Assert
        $this->assertEquals($expectedNumberOfContacts, $retrievedContacts->count());
    }
}
