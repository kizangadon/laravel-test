<?php

namespace Tests\Integration;

use App\Contact;
use App\Repository\ContactRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactRespositoryIntegrationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function the_contactRepository_retrievesTheExpectedNumberOfContacts_whenTheySearchForAContactsFirstname_And_SuchContactExists(){
        //Arrange
        $contacts = factory(Contact::class, 10)->create([
            'firstname' => $this->faker()->firstName
            ]);

        $searchFirstname = $contacts[0]->firstname;
        $expectedNumberOfContacts = 10;
        //Act
        $retrievedContacts = ContactRepository::search(new Contact(), 'firstname', $searchFirstname);
        //Assert
        $this->assertEquals($expectedNumberOfContacts, $retrievedContacts->count());
    }

    /**
     * @test
     */
    public function the_contactRepository_retrievesTheExpectedNumberOfContacts_whenTheySearchForAContactsLastname_And_SuchContactExists(){
        //Arrange
        $contacts = factory(Contact::class, 5)->create([
            'lastname' => $this->faker->lastName
            ]);

        $searchLastname = $contacts[0]->lastname;
        $expectedNumberOfContacts = 5;
        //Act
        $retrievedContacts = ContactRepository::search(new Contact(), 'lastname', $searchLastname);
        //Assert
        $this->assertEquals($expectedNumberOfContacts, $retrievedContacts->count());
    }

    /**
     * @test
     */
    public function the_contactRepository_retrievesTheExpectedNumberOfContacts_whenTheySearchForAContactsEmail_And_SuchContactExists(){
        //Arrange
        $contacts = factory(Contact::class, 5)->create([
            'email' => $this->faker->email
            ]);

        $searchEmail = $contacts[0]->email;
        $expectedNumberOfContacts = 5;
        //Act
        $retrievedContacts = ContactRepository::search(new Contact(), 'email', $searchEmail);
        //Assert
        $this->assertEquals($expectedNumberOfContacts, $retrievedContacts->count());
    }

    /**
     * @test
     */
    public function the_contactRepository_retrievesTheExpectedNumberOfContacts_whenTheySearchForAContactsContactNumber_And_SuchContactExists(){
        //Arrange
        $contacts = factory(Contact::class, 20)->create([
            'contact_number' => $this->faker->e164PhoneNumber
            ]);

        $searchContactNumber = $contacts[0]->contact_number;
        $expectedNumberOfContacts = 20;
        //Act
        $retrievedContacts = ContactRepository::search(new Contact(), 'contact_number', $searchContactNumber);
        //Assert
        $this->assertEquals($expectedNumberOfContacts, $retrievedContacts->count());
    }
}
