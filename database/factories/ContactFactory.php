<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contact;
use Faker\Generator as Faker;

$factory->define(Contact::class, function (Faker $faker) {
    $genderArray = [
        'Male', 'Female'
    ];

    $gender = $genderArray[rand(0, count($genderArray) - 1)];

    $function = "firstName" . $gender;

    $charactersToFindFromName = ['\'', '`'];

    return [
        'firstname' => $firstname = $faker->$function,
        'lastname' => $lastname = str_replace($charactersToFindFromName, '', $faker->lastname),
        'gender' => $gender,
        'email' => strtolower($firstname) . "@" . 'company' . ".com",
        'contact_number' => $faker->e164PhoneNumber
    ];
});
