@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h2 class="text-center">Contacts Management System</h2>
                </div>

                <div class="card-body">
                    <h3 class="text-center">Welcome @guest Guest @else{!!auth()->user()->name!!}@endguest</h3>

                    @guest
                        @include('partials.guest-home')
                    @endguest

                    @auth
                        @include('partials.user-home')
                    @endauth
                </div>

                <div class="card-footer text-muted">
                    <p class="text-center">Developed by Abangani Media</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
