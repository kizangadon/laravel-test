<p class="py-2">Since you are viewing this page, it's assumed that you have read the instructions on this <a href="https://kizangadon@bitbucket.org/kizangadon/laravel-test.git">page</a>.</p>
<p>This Contact Management System will allow you to do the following things:</p>

<blockquote>
    <ul>
        <li>Keep track of all your contacts.</li>
        <li>Add a new contact to your contact list.</li>
        <li>Update a contact's details in your contact list.</li>
        <li>Remove a contact from your contact list.</li>
    </ul>
</blockquote>
