<p class="lead py-2">Welcome back to your dashboard</p>

<p>The purpose of this application is to help you keep track of all your contacts.</p>

<p>You can quickly create a new contact by clicking <a href="{{route('create')}}">here</a>.</p>

<p>You can quickly go to your contacts overview by clicking <a href="{{route('contacts')}}">here</a>.</p>
