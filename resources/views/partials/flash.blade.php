<div class="row justify-content-center">
    <div class="col-md-10">
        @if(session('flash_error'))
            <div class="alert alert-danger" role="alert">
                <p>{{ session('flash_error') }}</p>
            </div>
        @elseif(session('flash_success'))
            <div class="alert alert-success" role="alert">
                <p>{!! session('flash_success') !!}</p>
            </div>
        @elseif(session('flash_info'))
            <div class="alert alert-info" role="alert">
                <p>{!! session('flash_info') !!}</p>
            </div>
        @endif
    </div>
</div>
