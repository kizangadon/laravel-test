@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
        <div class="col-md-10">
            <h2 class="text-center">View Contact</h2>

            <div class="py-4">
                <div class="card my-3">
                    <div class="card-header">
                        <p class="text-center py1"><small> This page displays information pertaining to a single contact.</small></p>
                    </div>

                    <div class="card-body">
                        <h4 class="card-title text-bold text-center">{{$contact->fullname()}}</h4>
                    </div>

                    <div class="card-text">
                        <table class="table table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th>Firstname</th>
                                    <td>{{ $contact->firstname }}</td>
                                </tr>
                                <tr>
                                    <th>Lastname</th>
                                    <td>{{ $contact->lastname }}</td>
                                </tr>
                                <tr>
                                    <th colspan="1">Gender</th>
                                    <td colspan="1">{{ $contact->gender }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $contact->email }}</td>
                                </tr>
                                <tr>
                                    <th>Contact Number</th>
                                    <td>{{ $contact->contact_number }}</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <table class="table table-hover">
                    <tfoot>
                        <tr>
                            <td>
                                <a href="{{route('edit', [$contact->id])}}" class="btn btn-outline-info">Edit {{$contact->firstname}}</a>
                            </td>
                            <td>
                                <form method="POST" action="{{ route('delete-contact', [$contact->id]) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-outline-danger float-right">Delete {{ $contact->firstname }}</button>
                                </form>
                            </td>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
@endsection
