@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-10">
        <h2 class="text-center">Search Contacts</h2>

        <p class="text-center py-2"><small>This page allows you to search for a particular content in your contact list.</small></p>

        <hr>

        <form method="POST" action="{{ route('search-contact') }}">
            @csrf

            <div class="form-group">
                <label for="criteria">Search Criteria</label>
                <select name="criteria" class="form-control @error('criteria') is-invalid @enderror" required>
                    <option value="firstname">Firstname</option>
                    <option value="lastname">Lastname</option>
                    <option value="email">Email</option>
                    <option value="contact_number">Contact Number</option>
                </select>

                @error('criteria')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group">
                <label for="value">Value</label>
                <input name="value" type="text" class="form-control @error('value') is-invalid @enderror" value="{{ old('value') }}" required />

                @error('value')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-dark">Search Contact</button>

                <a href="{{route('contacts')}}" class="btn btn-outline-success float-right">Return To Contacts Overview</a>
            </div>
        </form>
    </div>
</div>
@endsection
