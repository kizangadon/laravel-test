@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
        <div class="col-md-10">
            <h2 class="text-center">Search Results</h2>

            <p class="text-center py-3"><small>Your search returned {{ $contacts->count() }} result(s).</small></p>

            <table class="table">
                <thead>
                    <tr>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Email</th>
                        <th colspan="2"></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($contacts as $contact)
                    <tr>
                        <td>{{ $contact->firstname }}</td>
                        <td>{{ $contact->lastname }}</td>
                        <td>{{ $contact->email }}</td>
                        <td>
                            <a href="{{ route('view', [$contact->id]) }}" class="btn btn-outline-success btn-block btn-sm float-right">View</a>
                        </td>
                        <td>
                        <a href="{{ route('edit', [$contact->id]) }}" class="btn btn-outline-info btn-block btn-sm float-right">Edit</a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td class="text-center" colspan="5">Unfortunealy we could not find anything that matched your search.
                    </tr>
                    @endforelse
                </tbody>

                <tfoot>
                    <tr>
                        <td>
                            <a href="{{route('search')}}" class="btn btn-outline-info">Return To Search</a>
                        </td>
                        <td colspan="4">
                            <a href="{{route('contacts')}}" class="btn btn-outline-success float-right">Return To Contacts Overview</a>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
