@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
        <div class="col-md-10">
            <h2 class="text-center">Create New Contact</h2>

            <p class="text-center py-2"><small>This page allows you to create a new contact which you can keep track of.</small></p>

            <hr>

            <form method="POST" action="{{ route('create-contact') }}">
                @csrf
                <div class="form-group">
                    <label for="firstname">Firstname</label>
                    <input name="firstname" type="text" class="form-control @error('firstname') is-invalid @enderror" value="{{ old('firstname') }}" required />

                    @error('firstname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="lastname">Lastname</label>
                    <input name="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" value="{{ old('lastname') }}" required />

                    @error('lastname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="gender">Gender</label>
                    <select name="gender" class="form-control @error('gender') is-invalid @enderror" required>
                        <option value="Female">Female</option>
                        <option value="Male">Male</option>
                    </select>

                    @error('gender')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="Email">Email</label>
                    <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required />

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="contact_number">Contact Number</label>
                    <input name="contact_number" type="text" class="form-control @error('contact_number') is-invalid @enderror" value="{{ old('contact_number') }}" required />

                    @error('contact_number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-dark">Create Contact</button>

                    <a href="{{route('contacts')}}" class="btn btn-outline-success float-right">Return To Contacts Overview</a>
                </div>
            </form>
        </div>
    </div>
@endsection
