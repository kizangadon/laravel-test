@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
        <div class="col-md-10">
            <h2 class="text-center">Contacts Overview</h2>

            <p class="text-center py-3"><small>This page shows you a list of all the contacts that you have stored.</small></p>

            <table class="table">
                <thead>
                    <tr>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Email</th>
                        <th colspan="2"></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($contacts as $contact)
                    <tr>
                        <td>{{ $contact->firstname }}</td>
                        <td>{{ $contact->lastname }}</td>
                        <td>{{ $contact->email }}</td>
                        <td>
                            <a href="{{ route('view', [$contact->id]) }}" class="btn btn-outline-success btn-block btn-sm float-right">View</a>
                        </td>
                        <td>
                        <a href="{{ route('edit', [$contact->id]) }}" class="btn btn-outline-info btn-block btn-sm float-right">Edit</a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td>You don't have any contacts yet.</td>
                    </tr>
                    @endforelse
                </tbody>

                <tfoot>
                    <tr>
                        <td colspan="5">
                            {{ $contacts->links() }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="{{ route('create') }}" class="btn btn-sm btn-dark btn-block">Create Contact</a>
                        </td>
                        <td colspan="4"></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
