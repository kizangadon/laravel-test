@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
        <div class="col-md-10">
            <h2 class="text-center">Edit Contact</h2>

            <p class="text-center py-1"><small>This page allows you to modify the information of a contact.</small></p>

            <hr>

            <form method="POST" action="{{ route('update-contact', [$contact->id]) }}">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="firstname">Firstname</label>
                    <input name="firstname" type="text" value="{{ $contact->firstname }}" class="form-control @error('firstname') is-invalid @enderror" required />

                    @error('firstname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="lastname">Lastname</label>
                    <input name="lastname" type="text" value="{!! $contact->lastname !!}" class="form-control @error('lastname') is-invalid @enderror" required />

                    @error('lastname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="gender">Gender</label>
                    <select name="gender" class="form-control" disabled>
                        <option value="Female" {{ $contact->gender == "Female" ? 'selected' : ''}}>Female</option>
                        <option value="Male" {{ $contact->gender == "Male" ? 'selected' : ''}}>Male</option>
                    </select>

                    @error('gender')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="Email">Email</label>
                    <input name="email" type="email" value="{{ $contact->email }}" class="form-control @error('email') is-invalid @enderror" required />

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="contact_number">Contact Number</label>
                    <input name="contact_number" type="text" value="{{ $contact->contact_number }}" class="form-control @error('contact_number') is-invalid @enderror" required />

                    @error('contact_number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-dark">Update Contact</button>

                    <a href="{{route('contacts')}}" class="btn btn-outline-success float-right">Return To Contacts Overview</a>
                </div>
            </form>
        </div>
    </div>
@endsection
