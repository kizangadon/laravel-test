# Laravel Test

## Deliverables

1. Clear instructions on:
    1. How to test this project.
    2. How to build this project.
    3. How to run/execute this project.
2. Assumptions made for this project.
2. Brief discussion of the design decisions made for this project.

### Requirements

1. The project was created using the Visual Code (Version 1.41.1) IDE.
2. The project was created using the following technologies/tools:
    1. PHP 7.2.4
    2. Laravel 5.8
    3. Boostrap 4 (This comes packaged with Laravel 5.8)
    4. PHPUnit 7.5.20 (This comes packaged with Laravel 5.8)

### Installation

You will need to have PHP, MySQL and Git installed on your system. If you don't have PHP and MySQL installed, a suggestion would be [WAMP]("http://www.wampserver.com/en/") for windows, [MAMP]("https://www.mamp.info/en/downloads/") for MacOS and [LAMP]("https://bitnami.com/stack/lamp/installer") for Linux users. After ensuring that you have PHP, MySQL and Git installed, follow the following steps:

1. Open the command prompt (Windows users) or the terminal (MacOS and Linux users).
2. Locate to the directory where you normally store your laravel projects.
3. The type

```bash 
git clone https://kizangadon@bitbucket.org/kizangadon/laravel-test.git
cd laravel-test
```

### Usage

#### How to test the application

1. Open the command prompt (Windows) or terminal (MacOs and Linux).
2. Locate the laravel-test project directory.
3. The path to PHPUnit (Testing framework for this project) is your .\vendor\bin\phpunit (Windows) or ./vendor/bin/phpunit (MacOs and Linux users) assuming that you're already in the laravel-test project directory. You can either keep typing this command or create an alias for it, [windows]("https://helpdeskgeek.com/windows-10/add-windows-path-environment-variable/"), [mac]("http://osxdaily.com/2014/08/14/add-new-path-to-path-command-line/") or [linux]("https://www.techrepublic.com/article/how-to-add-directories-to-your-path-in-linux/").

##### Windows

```bash
.\vendor\bin\phpunit --testdox
```

##### MacOS & Linux

```bash
./vendor/bin/phpunit --testdox
```

#### How to build the application

The following steps will ensure that the required database that this project uses is created and sample data is created in the tables for you to use.

1.You will need to create the database and user for the application, open MySQL from the command prompt/terminal through whichever tool you use for managing your MySQL database. Assuming that you going to use the command prompt/terminal, then type:

```bash
mysql -u your_username -p your_password
CREATE DATABASE lara_test;
CREATE USER 'laratest'@'localhost' IDENTIFIED BY 'laratest';
GRANT ALL ON lara_test.* TO 'laratest'@'localhost';
```

2.This steps depends on you successfully completing the previous step. Assmuning that you're still in the command prompt/termina and still in the project's directory, type:

```bash
php artisan migrate --seed
```

Or

```bash
php artisan migrate
php artisan db:seed
```

#### How to run/execute the application

1.Open the command prompt (Windows) or terminal (MacOs/Linux).
2.Locate to the project's directory
3.Assuming that you don't have any service running on port 8000, Execute the following command

```bash
php artisan serve
```

If you have a service using port 8000, then execute the following

```bash
php artisan serve --port=enter_your_free_port
```

4.Open your favourite browser and enter the following address

```bash
http://localhost:8000 or http://localhost:enter_the_port_you_selected
```

## Assumptions

1.An assumption made for this project is that contacts do not belong to any particular user, so they all belong to the application. It was not clearly stated in the requirements as to who owns the contacts, so since this is a small project, I took the simple approach.
2.The major functionality for this application is the CRUD (Create, Read, Update and Delete) operations for a contact. Any other features will be the developer showcasing their skills.
3.The testor (person conducting this test) would prefer not to be sent an sql export of our records on the application and would prefer receiving sample records in an alternative manner.

## Design Decisions

1. The application uses the MVC (Model-View-Controller) design architecture pattern, as implemented by the Laravel framework.
2. The application was built using the [Test-Driven Development]("https://en.wikipedia.org/wiki/Test-driven_development") approach/methodology.
    1. The system has been tested on three levels:
        1. Feature/Controller Tests.
            1. Contact Features Test: These tests test the expected features we have of this project.
            2. Contact Validation Features Test: These tests test the validation constrains we have in this project.
            3. Views Features Test: These tests test that the views behave as expected in this project.
            4. Guest Exceptions Feature Test: These tests test the edge cases that a guest may encounter.
            5. User Exceptions Feature Test: These tests test the edge cases that a user (logged in) may encounter.
        2. Integration Tests.
            1. Database Seeder Integration Test: These tests test that when the database has been seeded it populates the expected table with the expected amount of records.
            2. Contact Repository Integration Test: These tests test that a contact can be searched from the databases by their details.
            3. Contact Service Integration Test: These tests test that a user can search for a contact by their details.
        3. Unit Tests.
            1. Contact Seeder Unit Test: These tests test that the contact seeder populates the contact database with the expected amount of records.
3. Code quality and maintainability was ensured using good Object-Oriented programming practices.
    1. SOLID principles were used were applicable in this project.
    2. Classes are loosely coupled and have high cohesion.
    3. Where instances of classes are needed, factories are mostly used to create them.
4. Extra search functionality. The application has an extra search functionality which can be accessed from using the **/search** route. It's not included on the dashboard as it was not requested. As of writing, validation on text search text has not been fully tested and implemented, so please take care when searching, thanks.

## Challenges
 
1. Deciding whether I should allow each user to have their own contacts or not.
2. Deciding on which extra features to add to the application whilst also being aware of the time constraints of submitting the project on time.
