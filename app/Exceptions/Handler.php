<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use InvalidArgumentException;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Creates a flash message for a user based on their request method
     *
     * @param  string  $method
     * @return array
     */
    protected function createFlashMessage($method){
        switch($method){
            case "GET":
                $flashMessage = "Dear user, the contact you attempted to view could not be found.";
                break;
            case "DELETE":
                $flashMessage = "Dear user, the contact you attempted to delete could not be found, please try again.";
                break;
            case "PUT":
                $flashMessage = "Dear user, the contact you attempted to update could not be found, please try again.";
                break;
            default:
                $flashMessage = "Dear user, the operation you attempted could not be processed, please try again.";
                break;
        }

        return [
            'flash_error' => $flashMessage
        ];
    }

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($exception instanceof NotFoundHttpException){
            $flashMessage = [
                'flash_error' => 'Dear user, the path that you requested could not be found, please try again.'
            ];

            return redirect(route('home'))->with($flashMessage);
        }

        if($exception instanceof NotFoundResourceException){
            $flashMessage = [
                'flash_error' => 'Dear user, the resource that you requested could not be found, please try again.'
            ];

            return redirect(route('home'))->with($flashMessage);
        }

        if($exception instanceof QueryException){
            $flashMessage = [
                'flash_error' => 'Dear user, it seems as if your database environment is not setup yet or properly. Please follow the instructions on our landing page on how to boot up the application, thank you.'
            ];

            return redirect(route('home'))->with($flashMessage);
        }

        if($exception instanceof ModelNotFoundException){
            return redirect()->route('contacts')->with($this->createFlashMessage($request->method()));
        }

        return parent::render($request, $exception);
    }
}
