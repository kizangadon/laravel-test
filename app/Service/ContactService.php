<?php

namespace App\Service;

use App\Contact;

use App\Repository\ContactRepository;
use App\Repository\IRepository;

class ContactService{
    protected $repository;

    public function __construct(IRepository $repository)
    {
        $this->repository = $repository;
    }

    public function createContact($firstname, $lastname, $gender, $email, $contactNumber){
        $contact = new Contact();

        $contact->firstname = $firstname;
        $contact->lastname = $lastname;
        $contact->gender = $gender;
        $contact->email = $email;
        $contact->contact_number = $contactNumber;

        return $this->repository::save($contact);
    }

    public function deleteContact(Contact $contact){
        $this->repository::delete($contact);
    }

    public function paginate(Contact $contact, $numberOfItems){
        return $this->repository::paginate($contact, $numberOfItems);
    }

    public function search($criteria, $value){
        $contacts = [];

        switch($criteria){
            case "firstname":
                $contacts = $this->searchByFirstname($value);
                break;
            case "lastname":
                $contacts = $this->searchByLastname($value);
                break;
            case "email":
                $contacts = $this->searchByEmail($value);
                break;
            case "contact_number":
                $contacts = $this->searchByContactNumber($value);
                break;
        }

        return $contacts;
    }

    public function searchByContactNumber($contactNumber){
        return $this->repository::search(new Contact(), 'contact_number', $contactNumber);
    }

    public function searchByEmail($email){
        return $this->repository::search(new Contact(), 'email', $email);
    }

    public function searchByFirstname($firstname){
        return $this->repository::search(new Contact(), 'firstname', $firstname);
    }

    public function searchByLastname($lastname){
        return $this->repository::search(new Contact(), 'lastname', $lastname);
    }

    public function updateContact(Contact $contact, $firstname, $lastname, $email, $contactNumber){
        $contact->firstname = $firstname;
        $contact->lastname = $lastname;
        $contact->email = $email;
        $contact->contact_number = $contactNumber;

        return $this->repository::save($contact);
    }
}
