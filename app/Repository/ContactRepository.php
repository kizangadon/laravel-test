<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;

class ContactRepository implements IRepository{
    public static function delete(Model $model){
        $model->delete();
    }

    public static function paginate(Model $model, $numberOfItems){
        return $model->paginate($numberOfItems);
    }

    public static function save(Model $model){
        $model->save();

        return $model;
    }

    public static function search(Model $model, $key, $value){
        return $model->where($key, $value)->get();
    }
}
