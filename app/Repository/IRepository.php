<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;

interface IRepository{
    public static function delete(Model $model);
    public static function paginate(Model $model, $numberOfItems);
    public static function save(Model $model);
    public static function search(Model $model, $key, $value);
}
