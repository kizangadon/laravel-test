<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Validation rules for the contact field
     * @return array
     */
    protected function contactNumberValidationRules(){
        return [
            'required',
            'regex:/^\+?[0-9]\d{9,14}$/'
        ];
    }

    /**
     * Validation rules for the email field
     * @return array
     */
    protected function emailValidationRules(){
        return [
            'required',
            'email'
        ];
    }

    /**
     * Validation rules for the firstname field
     * @return array
     */
    protected function firstnameValidationRules(){
        return [
            'required',
            'regex:/^[a-zA-Z]{3,100}$/'
        ];
    }

    /**
     * Validation rules for the gender field
     * @return array
     */
    protected function genderValidationRules(){
        return [
            'required',
            'in:Female,Male'
        ];
    }

    /**
     * Validation rules for the lastname field
     * @return array
     */
    protected function lastnameValidationRules(){
        return [
            'required',
            'regex:/^[a-zA-Z]{3,100}$/'
        ];
    }
}
