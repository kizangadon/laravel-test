<?php

namespace App\Http\Controllers;

use App\Contact;

use App\Http\Requests\CreateContactRequest;
use App\Http\Requests\SearchContactRequest;
use App\Http\Requests\UpdateContactRequest;

use App\Service\ContactService;

class ContactController extends Controller
{
    protected $contactService;

    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }



    public function create(){
        return view('contacts.create');
    }

    public function destroy(Contact $contact){
        $this->contactService->deleteContact($contact);

        $flashMessage = [
            'flash_info' => 'Dear user, you deleted a contact by the name of ' . $contact->fullname() . ' successfully'
        ];

        return redirect(route('contacts'))->with($flashMessage);
    }

    public function edit(Contact $contact){
        return view('contacts.edit', compact('contact'));
    }

    public function find(SearchContactRequest $request){
        $contacts = $this->contactService->search($request->criteria, $request->value);

        return view("contacts.search-results", compact('contacts'));
    }

    public function index(){
        $numberOfItemsToPaginate = 5;

        $contacts = $this->contactService->paginate(new Contact(), $numberOfItemsToPaginate);

        return view("contacts.index", compact('contacts'));
    }


    public function search(){
        return view('contacts.search');
    }

    public function show(Contact $contact){
        return view('contacts.show', compact('contact'));
    }

    public function store(CreateContactRequest $request){
        $contact = $this->contactService->createContact($request->firstname, $request->lastname, $request->gender, $request->email, $request->contact_number);

        $flashMessage = [
            'flash_success' => 'Dear user, you created a new contact by the name of ' . $contact->fullname() . ' successfully'
        ];

        return redirect(route('view', [$contact->id]))->with($flashMessage);
    }

    public function update(UpdateContactRequest $request, Contact $contact){
        $contact = $this->contactService->updateContact($contact, $request->firstname, $request->lastname, $request->email, $request->contact_number);

        $flashMessage = [
            'flash_info' => 'Dear user, you updated a contact by the name of ' . $contact->fullname() . ' successfully'
        ];

        return redirect(route('view', [$contact->id]))->with($flashMessage);
    }
}
