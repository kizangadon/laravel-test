<?php

namespace App\Providers;

use App\Repository\ContactRepository;
use App\Repository\IRepository;

use App\Service\ContactService;

use Illuminate\Support\ServiceProvider;

class ContactServiceProvider extends ServiceProvider
{
    /**
     * Container bindings that should be registered
     */
    public $bindings = [
        IRepository::class => ContactRepository::class
    ];
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ContactService::class, function(){
            return new ContactService($this->app->make(IRepository::class));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
